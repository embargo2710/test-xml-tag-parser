<?php

namespace app\models;

/**
 * Class XmlTag
 * @package app\models
 *
 * @property int $id
 * @property string $name
 * @property int $quantity
 * @property int $xml_id
 *
 * @property Xml $xml
 */
class XmlTag extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'xml_tags';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'quantity', 'xml_id'], 'required'],
            [['quantity', 'xml_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [
                ['xml_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Xml::className(),
                'targetAttribute' => ['xml_id' => 'id']
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя',
            'quantity' => 'Количество',
            'xml_id' => 'Xml ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getXml()
    {
        return $this->hasOne(Xml::className(), ['id' => 'xml_id']);
    }
}
