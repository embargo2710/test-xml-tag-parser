<?php

namespace app\models;

use yii\behaviors\TimestampBehavior;

/**
 * Class Xml
 * @package app\models
 *
 * @property int $id
 * @property string $name
 * @property int $created_at
 *
 * @property XmlTag[] $xmlTags
 */
class Xml extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'xml';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'updatedAtAttribute' => false,
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['created_at'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя',
            'created_at' => 'Дата создания',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getXmlTags()
    {
        return $this->hasMany(XmlTag::className(), ['xml_id' => 'id']);
    }
}
