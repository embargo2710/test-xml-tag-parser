<?php

namespace app\models;

use yii\base\Model;
use yii\web\UploadedFile;

/**
 * Class XmlForm
 * @package app\models
 */
class XmlForm extends Model
{
    /**
     * @var UploadedFile
     */
    public $file;

    /**
     * @var Xml
     */
    public $xml;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                ['file'],
                'file',
                'skipOnEmpty' => false,
                'extensions' => 'xml',
                'mimeTypes' => 'application/xml,text/xml',
            ],
        ];
    }

    /**
     * @return bool
     */
    public function save(): bool
    {
        if ($this->validate()) {
            $this->xml = new Xml();

            $this->xml->setAttributes([
                'name' => $this->file->getBaseName(),
            ]);

            return $this->xml->save();
        }

        return false;
    }
}
