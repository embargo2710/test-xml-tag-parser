<?php

use yii\db\Migration;

/**
 * Class m180219_191154_create_xml_tags
 */
class m180219_191154_create_xml_tags extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('xml_tags', [
            'id' => $this->primaryKey(),
            'xml_id' => $this->integer()->notNull(),
            'name' => $this->string(255)->notNull(),
            'quantity' => $this->integer()->notNull(),
        ]);

        $this->addForeignKey(
            'fk_xml_tags_to_xml',
            'xml_tags',
            'xml_id',
            'xml',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('xml_tags');
    }
}
