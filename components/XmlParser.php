<?php

namespace app\components;

use app\models\XmlTag;
use yii\base\Component;
use yii\db\Exception;
use yii\web\UploadedFile;

/**
 * Class XmlParser
 * @package app\components
 */
class XmlParser extends Component
{
    /**
     * @var UploadedFile
     */
    public $file;

    /**
     * @var \DOMDocument
     */
    public $xml;

    /**
     * XmlParser constructor.
     *
     * @param UploadedFile $file
     * @param array $config
     */
    public function __construct(UploadedFile $file, array $config = [])
    {
        $this->file = $file;

        parent::__construct($config);
    }

    /**
     * @return bool
     */
    public function validate(): bool
    {
        libxml_use_internal_errors(true);

        $this->xml = new \DOMDocument();

        $this->xml->validateOnParse = true;

        return $this->xml->load($this->file->tempName);
    }

    /**
     * @param int $primaryKey
     */
    public function saveTags(int $primaryKey)
    {
        $tags = [];
        $reader = new \XMLReader();
        $reader->XML($this->xml->saveXML());

        while ($reader->read()) {
            $element = $reader->expand();

            if ($element instanceof \DOMElement) {
                if ($element->nodeType === XML_ELEMENT_NODE && $element->hasChildNodes()) {
                    if (!isset($tags[$element->tagName])) {
                        $tags[$element->tagName] = 0;
                    }
                    $tags[$element->tagName]++;
                }
            }
        }

        foreach ($tags as $tag => $quantity) {
            $model = new XmlTag();

            $model->setAttributes([
                'xml_id' => $primaryKey,
                'name' => $tag,
                'quantity' => $quantity,
            ]);

            if (!$model->save()) {
                throw new Exception('Не удалось сохранить данные о теге: ' . $model->getFirstErrors());
            }
        }
    }
}
