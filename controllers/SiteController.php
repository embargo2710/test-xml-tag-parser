<?php

namespace app\controllers;

use app\components\XmlParser;
use app\models\Xml;
use app\models\XmlForm;
use app\models\XmlTag;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

/**
 * Class SiteController
 * @package app\controllers
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $model = new XmlForm();

        if ($model->load(\Yii::$app->request->post())) {
            $file = UploadedFile::getInstance($model, 'file');
            $parser = new XmlParser($file);

            if ($parser->validate()) {
                $model->file = $file;

                if ($model->save()) {
                    $parser->saveTags($model->xml->getPrimaryKey());

                    \Yii::$app->session->addFlash('success', 'Файл успешно обработан');
                }
            } else {
                \Yii::$app->session->addFlash('error', 'Файл невалиден');
            }
        }

        $dataProvider = new ActiveDataProvider([
            'query' => Xml::find(),
            'pagination' => [
                'pageSize' => 10
            ]
        ]);

        $topXmlCount = Xml::find()->andWhere([
            'id' => XmlTag::find()
                ->groupBy('xml_id')
                ->select(['xml_id'])
                ->having(['>', new Expression('SUM(quantity)'), 20])
        ])->count();

        return $this->render('index', [
            'model' => $model,
            'dataProvider' => $dataProvider,
            'topXmlCount' => $topXmlCount,
        ]);
    }

    /**
     * @param int $id
     *
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionFile(int $id)
    {
        $model = Xml::findOne($id);

        if (!$model) {
            throw new NotFoundHttpException('Модель не найдена');
        }

        $dataProvider = new ActiveDataProvider([
            'query' => XmlTag::find()->andWhere(['xml_id' => $id])->orderBy(['name' => SORT_ASC])
        ]);

        return $this->render('file', [
            'xml' => $model,
            'dataProvider' => $dataProvider,
        ]);
    }
}
