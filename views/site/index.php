<?php

/**
 * @var $this yii\web\View
 * @var \app\models\XmlForm $model
 * @var \yii\data\ActiveDataProvider $dataProvider
 * @var int $topXmlCount
 */

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\grid\GridView;

?>

<div class="site-index">

    <div class="body-content">

        <?php
        $form = ActiveForm::begin();

        echo $form->field($model, 'file')->fileInput();

        echo Html::submitButton('Загрузить');

        ActiveForm::end();
        ?>

        <br>

        <?php
        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                'id',
                [
                    'attribute' => 'name',
                    'value' => function ($data) {
                        return Html::a($data->name, ['file', 'id' => $data->id], ['target' => 'file_' . $data->id]);
                    },
                    'format' => 'html',
                ],
                'created_at:datetime',
            ]
        ]);
        ?>

        <h2>Количество файлов, у которых тегов больше 20: <?= $topXmlCount; ?></h2>

    </div>

</div>
