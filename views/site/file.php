<?php

/**
 * @var $this yii\web\View
 * @var \yii\data\ActiveDataProvider $dataProvider
 * @var \app\models\Xml $xml
 */

use yii\grid\GridView;

?>

<div class="site-index">

    <div class="body-content">

        <h1>Статистика по файлу: <?= $xml->name ?></h1>

        <?php
        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                'id',
                'name',
                'quantity',
            ]
        ]);
        ?>

    </div>
</div>
